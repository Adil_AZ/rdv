﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RDV.Models;
using RDV.Database;
using Microsoft.AspNetCore.Authorization;

namespace RDV.Controllers
{
    [Authorize(Roles = "super")]
    [Route("[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        List<Admin> admins;
        AdminDatabase database;
        ErrorHandler errorHandler;

        // GET: Admin
        [HttpGet]
        public IActionResult Get()
        {
            database = new AdminDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            admins = database.GetAdmin(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (admins.Count == 0)
                return Ok(errorHandler);
            return Ok(admins);
        }

        // GET: Admin/5
        [HttpGet("{id}", Name = "GetAdmin")]
        public IActionResult Get(int id)
        {
            database = new AdminDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            admins = database.GetAdminById(id, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (admins.Count == 0)
                return Ok(errorHandler);
            return Ok(admins);
        }

        // POST: Admin
        [HttpPost]
        public JsonResult Post([FromBody] Admin value)
        {
            database = new AdminDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.SetAdmin(value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // PUT: Admin/5
        [HttpPut("{id}")]
        public JsonResult Put(int id, [FromBody] Admin value)
        {
            database = new AdminDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.EditAdmin(id, value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // DELETE: ApiWithActions/5
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            database = new AdminDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.DeleteAdmin(id);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }
    }
}
