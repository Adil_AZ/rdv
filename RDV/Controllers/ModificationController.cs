﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RDV.Database;
using RDV.Models;

namespace RDV.Controllers
{
    [Authorize(Roles = "super, admin")]
    [Route("[controller]")]
    [ApiController]
    public class ModificationController : ControllerBase
    {
        List<Modification> modifications;
        ModificationDatabase database;
        ErrorHandler errorHandler;

        // GET: api/Modification
        [HttpGet]
        public IActionResult Get()
        {
            database = new ModificationDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            modifications = database.GetModification(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (modifications.Count == 0)
                return Ok(errorHandler);
            return Ok(modifications);
        }

        // GET: api/Modification/{date}
        [HttpGet("{date}")]
        public IActionResult Get(string date)
        {
            database = new ModificationDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            modifications = database.GetModificationByDate(date,out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (modifications.Count == 0)
                return Ok(errorHandler);
            return Ok(modifications);
        }

        // GET: api/Modification/5
        [HttpGet("{firstDate}/{lastDate}")]
        public IActionResult Get(string firstDate, string lastDate)
        {
            database = new ModificationDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            modifications = database.GetModificationByRangeOfDate(firstDate, lastDate, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (modifications.Count == 0)
                return Ok(errorHandler);
            return Ok(modifications);

        }

        // POST: api/Modification
        [HttpPost]
        public JsonResult Post([FromBody] Modification value)
        {
            database = new ModificationDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.SetModification(value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // PUT: api/Modification/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
