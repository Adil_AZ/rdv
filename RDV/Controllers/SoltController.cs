﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using RDV.Database;
using RDV.Models;

namespace RDV.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class SoltController : ControllerBase
    {
        List<Solt> solts;
        SoltDatabase database;
        ErrorHandler errorHandler;

        // GET: api/Solt
        [Authorize(Roles = "super, admin")]
        [HttpGet]
        public IActionResult Get()
        {
            database = new SoltDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            solts = database.GetSolt(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (solts.Count == 0)
                return Ok(errorHandler);
            return Ok(solts);
        }

        // GET: api/Solt/useful
        [Authorize(Roles = "super, admin")]
        [HttpGet("useful")]
        public IActionResult GetUseful()
        {
            database = new SoltDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            solts = database.GetUsefulSolt(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (solts.Count == 0)
                return Ok(errorHandler);
            return Ok(solts);
        }

        // GET: api/Solt/activesolt/date
        [HttpGet("activesolt/{date}")]
        public IActionResult Get(string date)
        {
            database = new SoltDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            solts = database.GetActiveSolt(date,true, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (solts.Count == 0)
                return Ok(errorHandler);
            return Ok(solts);
        }

        // GET: api/Solt/{state}
        [Authorize(Roles = "super, admin")]
        [HttpGet("{state}")]
        public IActionResult GetByState(string state)
        {
            database = new SoltDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            solts = database.GetSoltByState(Convert.ToBoolean(state), out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (solts.Count == 0)
                return Ok(errorHandler);
            return Ok(solts);
        }

        

        // POST: api/Solt
       /* [HttpPost]
        public JsonResult Post([FromBody] Solt value)
        {
            database = new SoltDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.SetSolt(value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // PUT: api/Solt/5
        [HttpPut("{id}")]
        public JsonResult Put(int id, [FromBody] bool value)
        {
            database = new SoltDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.EditSoltState(id, value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
