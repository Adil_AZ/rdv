﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RDV.Database;
using RDV.Models;

namespace RDV.Controllers
{
    [Authorize(Roles = "super, admin")]
    [Route("[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        List<Setting> settings;
        SettingDatabase database;
        ErrorHandler errorHandler;

        // GET: Setting/active
        [HttpGet("active")]
        public IActionResult GetActive()
        {
            database = new SettingDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            settings = database.GetActiveSetting(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (settings.Count == 0)
                return Ok(errorHandler);
            return Ok(settings);
        }

        // GET: Setting/initial
        [HttpGet("initial")]
        public IActionResult GetInitial()
        {
            database = new SettingDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            settings = database.GetInitialSetting(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (settings.Count == 0)
                return Ok(errorHandler);
            return Ok(settings);
        }

        // GET: Setting/5
        [HttpGet("{Date}", Name = "GetSetting")]
        public IActionResult Get(string date)
        {
            database = new SettingDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            settings = database.GetSettingByModificationDate(date, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (settings.Count == 0)
                return Ok(errorHandler);
            return Ok(settings);
        }

        // POST: Setting
        [HttpPost]
        public JsonResult Post([FromBody] List<Setting> value)
        {
            database = new SettingDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.SetSetting(value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // PUT: Setting/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
