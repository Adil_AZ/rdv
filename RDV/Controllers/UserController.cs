﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using RDV.Database;
using RDV.Models;

namespace RDV.Controllers
{
    [Authorize(Roles = "super, admin")]
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        List<User> users;
        UserDatabase database;
        ErrorHandler errorHandler;


        // GET: api/User/onhold
        
        [HttpGet("onhold")]
        public IActionResult GetOnHold()
        {
            database = new UserDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            users = database.GetOnHoldUser(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (users.Count == 0)
                return Ok(errorHandler);
            return Ok(users);
        }
        [HttpGet]
        // GET: api/User
        public IActionResult Get()
        {
            database = new UserDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            users = database.GetUser(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (users.Count == 0)
                return Ok(errorHandler);
            return Ok(users);
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "GetUser")]
        public IActionResult Get(int id)
        {
            database = new UserDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            users = database.GetUserById(id, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (users.Count == 0)
                return Ok(errorHandler);
            return Ok(users);
        }

        // POST: api/User
        [HttpPost]
        public JsonResult Post([FromBody] User value)
        {
            database = new UserDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.SetUser(value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // PUT: api/User/onhold/5
        [HttpPut("onhold/{id}")]
        public JsonResult PutState(int id, [FromBody] string value)
        {
            database = new UserDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.EditUserState(id, value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public JsonResult Put(int id, [FromBody] User value)
        {
            database = new UserDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.EditUser(id, value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            database = new UserDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.DeleteUser(id);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }
    }
}
