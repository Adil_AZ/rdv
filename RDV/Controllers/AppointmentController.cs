﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RDV.Database;
using RDV.Models;

namespace RDV.Controllers
{
    [Authorize(Roles = "super, admin")]
    [Route("[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        List<Appointment> appointments;
        AppointmentDatabase database;
        ErrorHandler errorHandler;

        // GET: Appointment
        [HttpGet]
        public IActionResult Get()
        {
            database = new AppointmentDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            appointments = database.GetAppointment(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (appointments.Count == 0)
                return Ok(errorHandler);
            return Ok(appointments);
        }

        // GET: Appointment/{date}
        [HttpGet("{date}")]
        public IActionResult Get(string date)
        {
            database = new AppointmentDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            appointments = database.GetAppointmentByDate(date, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (appointments.Count == 0)
                return Ok(errorHandler);
            return Ok(appointments);
        }

        // GET: Appointment/{firstDate}/{lastDate}
        [HttpGet("{firstDate}/{lastDate}")]
        public IActionResult GetOnHold(string firstDate, string lastDate)
        {
            database = new AppointmentDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            appointments = database.GetAppointmentByRangeOfDate(firstDate, lastDate, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (appointments.Count == 0)
                return Ok(errorHandler);
            return Ok(appointments);
        }

        // GET: Appointment/onhold
        [HttpGet("onhold")]
        public IActionResult GetOnHold()
        {
            database = new AppointmentDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            appointments = database.GetOnHoldAppointment(out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (appointments.Count == 0)
                return Ok(errorHandler);
            return Ok(appointments);
        }

        // POST: Appointment
        [HttpPost]
        public JsonResult Post([FromBody] Appointment value)
        {
            database = new AppointmentDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.SetAppointment(value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // PUT: Appointment/5
        [HttpPut("{id}")]
        public JsonResult Put(int id, [FromBody] string value)
        {
            database = new AppointmentDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            errorHandler.databaseMessage = database.EditAppointmentState(id, value);
            database.CloseConnection();
            return new JsonResult(errorHandler);
        }

        // DELETE: ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
