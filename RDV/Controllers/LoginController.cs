﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RDV.Database;
using RDV.Models;

namespace RDV.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        User user;
        Admin admin;
        LoginDatabase database;
        ErrorHandler errorHandler;

        [AllowAnonymous]
        [HttpPost("user")]
        public IActionResult UserLogin([FromBody]Login value)
        {
            database = new LoginDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            user = database.UserAuthenticate(value, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (user == null)
                return Ok(errorHandler);
            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("admin")]
        public IActionResult AdminLogin([FromBody]Login value)
        {
            database = new LoginDatabase();
            errorHandler = new ErrorHandler();
            errorHandler.connectionMessage = database.OpenConnection();
            admin = database.AdminAuthenticate(value, out string databaseMessage);
            errorHandler.databaseMessage = databaseMessage;
            database.CloseConnection();
            if (admin == null)
                return Ok(errorHandler);
            return Ok(admin);
        }
        
    }
}
