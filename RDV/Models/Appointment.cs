﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Models
{
    public class Appointment
    {
        public int idAdmin { get; set; }
        public int idUser { get; set; }
        public int idSolt { get; set; }

        private string _appointmentDate;
        public string appointmentDate
        {
            get => _appointmentDate;
            set
            {
                _appointmentDate = Convert.ToDateTime(value).Date.ToString("yyyy-MM-dd");
            }
        }
        private string _requestDate;
        public string requestDate
        {
            get => _requestDate;
            set
            {
                _requestDate = Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public string appointmentState { get; set; }
    }
}
