﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Models
{
    //Editeur : Adil Arhrimaz
    //Description : Cette classe administrateur pour l'application systeme itelligent de gestion de rendez vous
    public class Admin
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

        private string _birthDate;
        public string birthDate 
        {
            get => _birthDate;
            set
            {
                _birthDate = Convert.ToDateTime(value).Date.ToString("yyyy-MM-dd");
            }
        }
        public string numberPhone { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string role { get; set; }
        public string token { get; set; }

    }
}
