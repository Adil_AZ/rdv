﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Models
{
    //Editeur : Adil Arhrimaz
    //Description : Cette classe personne pour l'application systeme itelligent de gestion de rendez vous
    public class User : Admin
    {
        public byte numberReservation { get; set; }
        public byte numberMissedAppointment { get; set; }
        public string userState { get; set; }
        public string stateDescription { get; set; }
    }
}
