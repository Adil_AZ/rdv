﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Models
{
    //Editeur : Adil Arhrimaz
    //Description : Cette classe parametres pour l'application systeme itelligent de gestion de rendez vous
    public class Setting
    {

        public int id { get; set; }
        public string name { get; set; }
        public List<string> values { get; set; }

        private string _addedDate;
        public string addedDate
        {
            get => _addedDate;
            set
            {
                _addedDate = Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
    }
}
