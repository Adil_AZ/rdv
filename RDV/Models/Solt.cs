﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Models
{
    public class Solt
    {
        public int idSolt { get; set; }
        public int duration { get; set; }
        private string _durationStart;
        public string durationStart
        {
            get => _durationStart;
            set
            {
                _durationStart = Convert.ToDateTime(value).ToString("HH:mm:ss");
            }
        }
        public byte percentage { get; set; }
        public bool soltState { get; set; }
    }
}
