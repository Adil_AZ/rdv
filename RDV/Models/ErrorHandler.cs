﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Models
{
    public class ErrorHandler
    {
        public string connectionMessage { get; set; }
        public string databaseMessage { get; set; }
    }
}
