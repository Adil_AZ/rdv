﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Models
{
    //Editeur : Adil Arhrimaz
    //Description : Cette classe modification pour l'application systeme itelligent de gestion de rendez vous
    public class Modification
    {
        public List<int> idSetting { get; set; }
        public int idAdmin { get; set; }

        private string _modificationDate;
        public string modificationDate
        {
            get => _modificationDate;
            set
            {
                _modificationDate = Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
    }
}
