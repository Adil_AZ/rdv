﻿using MySql.Data.MySqlClient;
using RDV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Database
{
    public class AppointmentDatabase
    {
        // declaration des variables
        private MySqlConnection connection;
        private string connectionString;
        List<Appointment> appointments;
        Appointment appointment;

        public AppointmentDatabase()
        {
            connectionString = "server=localhost;user=root;password=I@mgoingincounterstrike1.6fin@L;database=rdv";
            //connectionString = "server=localhost;user=root;password=;database=rdv";
            connection = new MySqlConnection(connectionString);
        }

        public List<Appointment> GetAppointment(out string databaseMessage)
        {
            string command = "SELECT * FROM rdv;";
            return GetMethod(command, out databaseMessage);
        }

        public List<Appointment> GetOnHoldAppointment(out string databaseMessage)
        {
            string command = "SELECT * FROM rdv WHERE etat_rdv = 'on hold';";
            return GetMethod(command, out databaseMessage);
        }

        public List<Appointment> GetAppointmentByDate(string date, out string databaseMessage)
        {
            string command = "SELECT * FROM rdv WHERE date_rdv = '" + date + "';"; 
            return GetMethod(command, out databaseMessage);
        }

        public List<Appointment> GetAppointmentByRangeOfDate(string firstDate, string lastDate, out string databaseMessage)
        {
            string command = "SELECT * FROM rdv WHERE date_rdv >=  '" + firstDate + "' AND date_rdv <=  '" + lastDate + "';";
            return GetMethod(command, out databaseMessage);
        }

        public string SetAppointment(Appointment appointment)
        {
            string command = "INSERT INTO `rdv`(`id_administrateur`, `id_utilisateur`, `id_creneau`, `date_rdv`, `etat_rdv`) VALUES ("+appointment.idAdmin+","+appointment.idUser+","+appointment.idSolt+",'"+appointment.appointmentDate + "','"+appointment.appointmentState+"');";
            return SetMethod(command);
        }

        public string EditAppointmentState(int id , string appointment)
        {
            string command = "UPDATE `rdv` SET `etat_rdv`='" + appointment + "' WHERE id_rdv = " + id + ";";
            if(appointment == "accepted")
                RefuseAppointment(id);
            return SetMethod(command);
        }

        public void RefuseAppointment(int id)
        {
            var idSolt = 0;
            var dateAppointment = "";
            using var command = new MySqlCommand("SELECT id_creneau, date_rdv FROM rdv WHERE id_rdv = " + id + ";", connection);
            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                idSolt = Convert.ToInt32(reader.GetValue(0));
                dateAppointment = reader.GetValue(1).ToString();
            }
            SetMethod("UPDATE rdv SET etat_rdv = 'refused' WHERE date_rdv = '"+dateAppointment+"' AND id_creneau = "+idSolt+" ;");

        }
       

        private List<Appointment> GetMethod(string commandd, out string databaseMessage)
        {

            appointments = new List<Appointment>();

            using var command = new MySqlCommand(commandd, connection);
            try 
            { 
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    appointment = new Appointment();
                    appointment.idAdmin = Convert.ToInt32(reader.GetValue(1));
                    appointment.idUser = Convert.ToInt32(reader.GetValue(2));
                    appointment.idSolt = Convert.ToInt32(reader.GetValue(3));
                    appointment.appointmentDate = reader.GetValue(4).ToString();
                    appointment.requestDate = reader.GetValue(5).ToString();
                    appointment.appointmentState = reader.GetValue(6).ToString();
                    appointments.Add(appointment);
                }
                databaseMessage = "Appointment Process done with success.";
            }
            catch (Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return appointments;
        }

        private string SetMethod(string commandd)
        {
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                command.ExecuteNonQuery();
                return "Appointment Added / Edited / Deleted Successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //ouverture de la connexion 
        public string OpenConnection()
        {
            try
            {
                connection.Open();
                return "Successful Connection.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //fermeture de la connexion
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
