﻿using MySql.Data.MySqlClient;
using RDV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Database
{
    
    //Editeur : Adil Arhrimaz
    //Description : Cette classe adminDatabase permet la connexion et l'utilisation de ses fonctions CRUD entre Base de donnee et la classe administrateur
    public class UserDatabase
    {
        //declaration des variables
        private MySqlConnection connection;
        private string connectionString;
        List<User> users;
        User user;
        public UserDatabase()
        {
            //connectionString = "server=localhost;user=root;password=I@mgoingincounterstrike1.6fin@L;database=rdv";
            connectionString = "server=localhost;user=root;password=;database=rdv";
            connection = new MySqlConnection(connectionString);
        }

        //fonction de recuperation des personnes
        public List<User> GetUser(out string databaseMessage)
        {
            string command = "SELECT * FROM utilisateur;";
            return GetMethod(command, out databaseMessage);
        }

        public List<User> GetOnHoldUser(out string databaseMessage)
        {
            string command = "SELECT * FROM `utilisateur` WHERE utilisateur_etat = 'on hold' ;";
            return GetMethod(command, out databaseMessage);
        }

        //fonction  de recuperation des personnes par l'id
        public List<User> GetUserById(int id, out string databaseMessage)
        {
            string command = "SELECT * FROM utilisateur WHERE id_utilisateur = " + id + ";";
            return GetMethod(command, out databaseMessage);
        }

        //fonction d'insertion des personnes
        public string SetUser(User user)
        {
            var check = UserExist(user);
            if (check == 0)
            {
                string command = "INSERT INTO `utilisateur`(`nom`, `prenom`, `date_naissance`, `tele`, `email`, `mot_de_passe`, `role`, `nb_reservation`, `nb_rdv_manque`, `utilisateur_etat`, `description_etat`) VALUES ('" + user.lastName + "','" + user.firstName + "','" + user.birthDate + "','" + user.numberPhone + "','" + user.email + "','" + user.password + "','user'," + user.numberReservation + "," + user.numberMissedAppointment + ",'" + user.userState + "','" + user.stateDescription + "')";
                return SetMethod(command);
            }
            return "User Already Exist.";
        }

        // fonction de modification des personnes
        public string EditUser(int id, User user)
        {
            string command = "UPDATE `utilisateur` SET `nom` = '" + user.lastName + "', `prenom` = '" + user.firstName + "', `date_naissance` = '" + user.birthDate + "', `tele` = '" + user.numberPhone + "', `email` = '" + user.email + "', `mot_de_passe` = '" + user.password + "' WHERE `id_utilisateur` = " + id + ";";
            return SetMethod(command);
        }

        public string EditUserState(int id, string state)
        {
            string command = "UPDATE `utilisateur` SET utilisateur_etat = '" + state + "' WHERE `id_utilisateur` = " + id + ";";
            return SetMethod(command);
        }

        // fonction de suppresion des personnes
        public string DeleteUser(int id)
        {
            string command = "DELETE FROM `utilisateur` WHERE `id_utilisateur` = " + id + ";";
            return SetMethod(command);
        }

        private int UserExist(User user)
        {
            var check = 0;
            using var command = new MySqlCommand("SELECT COUNT(*) FROM `utilisateur` WHERE email = '" + user.email + "';", connection);
            try
            {
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    check = Convert.ToInt32(reader.GetValue(0));
                }
            }
            catch (Exception)
            {
                return check;
            }
            return check;
        }

        private List<User> GetMethod(string commandd, out string databaseMessage)
        {

            users = new List<User>();
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    user = new User();
                    user.id = Convert.ToInt32(reader.GetValue(0));
                    user.lastName = reader.GetValue(1).ToString();
                    user.firstName = reader.GetValue(2).ToString();
                    user.birthDate = reader.GetValue(3).ToString();
                    user.numberPhone = reader.GetValue(4).ToString();
                    user.email = reader.GetValue(5).ToString();
                    user.role = reader.GetValue(7).ToString();
                    user.numberReservation = Convert.ToByte(reader.GetValue(8));
                    user.numberMissedAppointment = Convert.ToByte(reader.GetValue(9));
                    user.userState = reader.GetValue(10).ToString();
                    user.stateDescription = reader.GetValue(11).ToString();
                    users.Add(user);
                }
                databaseMessage = "User Process done with success.";
            }
            catch(Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return users;
        }

        private string SetMethod(string commandd)
        {
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                command.ExecuteNonQuery();
                return "User Added / Edited / Deleted Successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        //ouverture de la connexion
        public string OpenConnection()
        {
            try
            {
                connection.Open();
                return "Successful Connection.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //fermeture de la connexion
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
