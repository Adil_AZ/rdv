﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RDV.Models;

namespace RDV.Database
{
    //Editeur : Adil Arhrimaz
    //Description : Cette classe adminDatabase permet la connexion et l'utilisation de ses fonctions CRUD entre Base de donnee et la classe administrateur
    public class AdminDatabase
    {
        // declaration des variables
        private MySqlConnection connection;
        private string connectionString;
        List<Admin> admins;
        Admin admin;

        public AdminDatabase()
        {
            connectionString = "server=localhost;user=root;password=I@mgoingincounterstrike1.6fin@L;database=rdv";
            //connectionString = "server=localhost;user=root;password=;database=rdv";
            connection = new MySqlConnection(connectionString);
        }

        //fonction de recuperation des administrateurs
        public List<Admin> GetAdmin(out string databaseMessage)
        {
            string command = "SELECT * FROM administrateur;";
            return GetMethod(command, out databaseMessage);
        }

        //fonction  de recuperation des administrateurs par l'id
        public List<Admin> GetAdminById(int id, out string databaseMessage)
        {
            string command = "SELECT * FROM administrateur WHERE id_administrateur = " + id + ";";
            return GetMethod(command, out databaseMessage);
        }

        //fonction d'insertion des administrateurs
        public string SetAdmin(Admin admin)
        {
            var check = AdminExist(admin);
            if (check == 0)
            {
                string command = "INSERT INTO `administrateur`(`nom`, `prenom`, `date_naissance`, `tele`, `email`, `mot_de_passe`, `role`) VALUES ('" + admin.lastName + "','" + admin.firstName + "','" + admin.birthDate + "','" + admin.numberPhone + "','" + admin.email + "','" + admin.password + "','admin')";
                return SetMethod(command);
            }
            return "Admin Already Exist.";
        }

        //fonction de modification des administrateurs
        public string EditAdmin(int id , Admin admin)
        {
            string command = "UPDATE `administrateur` SET `nom` = '" + admin.lastName + "', `prenom` = '" + admin.firstName + "', `date_naissance` = '" + admin.birthDate + "', `tele` = '" + admin.numberPhone + "', `email` = '" + admin.email + "', `mot_de_passe` = '" + admin.password + "' WHERE `id_administrateur` = " + id + ";";
            return SetMethod(command);
        }

        // fonction de suppresion des administrateurs
        public string DeleteAdmin(int id)
        {
            string command = "DELETE FROM `administrateur` WHERE `id_administrateur` = " + id + ";";
            return SetMethod(command);
        }

        private int AdminExist(Admin user)
        {
            var check = 0;
            using var command = new MySqlCommand("SELECT COUNT(*) FROM `Administrateur` WHERE email = '" + user.email + "';", connection);
            try
            {
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    check = Convert.ToInt32(reader.GetValue(0));
                }
            }
            catch (Exception)
            {
                return check;
            }
            return check;
        }

        private List<Admin> GetMethod(string commandd, out string databaseMessage)
        {

            admins = new List<Admin>();
            try
            {

                using var command = new MySqlCommand(commandd, connection);
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    admin = new User();
                    admin.id = Convert.ToInt32(reader.GetValue(0));
                    admin.lastName = reader.GetValue(1).ToString();
                    admin.firstName = reader.GetValue(2).ToString();
                    admin.birthDate = reader.GetValue(3).ToString();
                    admin.numberPhone = reader.GetValue(4).ToString();
                    admin.email = reader.GetValue(5).ToString();
                    admin.role = reader.GetValue(7).ToString();
                    admins.Add(admin);
                }
                databaseMessage = "Admin Process done with success.";
            }
            catch (Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return admins;
        }

        private string SetMethod(string commandd)
        {
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                command.ExecuteNonQuery();
                return "Admin Added / Edited / Deleted Successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        //ouverture de la connexion 
        public string OpenConnection()
        {
            try
            {
                connection.Open();
                return "Successful Connection.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //fermeture de la connexion
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
