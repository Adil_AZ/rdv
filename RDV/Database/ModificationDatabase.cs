﻿using MySql.Data.MySqlClient;
using RDV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Database
{
        //Editeur : Adil Arhrimaz
        //Description : Cette classe modificationDatabase permet la connexion et l'utilisation de ses fonctions CRUD entre Base de donnee et la classe modification
    public class ModificationDatabase
    {
        // declaration des variables
        private MySqlConnection connection;
        private string connectionString;
        List<Modification> modifications;
        Modification modification;
        public ModificationDatabase()
        {
            connectionString = "server=localhost;user=root;password=I@mgoingincounterstrike1.6fin@L;database=rdv";
            //connectionString = "server=localhost;user=root;password=;database=rdv";
            connection = new MySqlConnection(connectionString);
        }

        //fonction de recuperation des modifications
        public List<Modification> GetModification(out string databaseMessage)
        {
            string command = "SELECT * FROM modification;";
            return GetMethod(command, out databaseMessage);
        }
        public List<Modification> GetModificationByDate(string date, out string databaseMessage)
        {
            string command = "SELECT * FROM modification WHERE date_modification = '" + date + "' ;";
            return GetMethod(command, out databaseMessage);
        }

        public List<Modification> GetModificationByRangeOfDate(string firstDate, string lastDate, out string databaseMessage)
        {
            string command = "SELECT * FROM modification WHERE date_modification >=  '" + firstDate + "' AND date_modification <=  '" + lastDate + "';";
            return GetMethod(command, out databaseMessage);
        }


        //fonction d'insertion des modifications
        public string SetModification(Modification modification)
        {
            string command = "INSERT INTO `modification`(`id_parametre`, `id_administrateur`, `date_modification`) VALUES ("+modification.idSetting+","+modification.idAdmin+",'"+modification.modificationDate+"');";
            return SetMethod(command);
        }

        

        

        private List<Modification> GetMethod(string commandd, out string databaseMessage)
        {

            modifications = new List<Modification>();

            using var command = new MySqlCommand(commandd, connection);
            try
            {
                using var reader = command.ExecuteReader();
                var idSetting = new List<int>();
                bool check = false;
                string date = null;
                while (reader.Read())
                {
                    if (date != reader.GetValue(3).ToString())
                    {
                        if (check)
                        {
                            modification.idSetting = idSetting;
                            modifications.Add(modification);
                        }
                        idSetting = new List<int>();
                        modification = new Modification();
                        idSetting.Add(Convert.ToInt32(reader.GetValue(1)));
                        modification.idAdmin = Convert.ToInt32(reader.GetValue(2));
                        modification.modificationDate = reader.GetValue(3).ToString();
                        date = reader.GetValue(3).ToString();
                        check = true;
                    }
                    else
                    {
                        idSetting.Add(Convert.ToInt32(reader.GetValue(1)));
                    }
                }
                modification.idSetting = idSetting;
                databaseMessage = "Modification Process done with success.";
            }
            catch (Exception ex)
            {
                databaseMessage = ex.Message;
            }
            modifications.Add(modification);
            return modifications;
        }

        private string SetMethod(string commandd)
        {
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                command.ExecuteNonQuery();
                return "modification Added / Edited / Deleted Successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        //ouverture de la connexion

        public string OpenConnection()
        {
            try
            {
                connection.Open();
                return "Successful Connection.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //fermeture de la connexion
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
