﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using RDV.Models;

namespace RDV.Database
{

    public interface ILoginDatabase
    {
        User UserAuthenticate(Login login, out string databaseMessage);
        Admin AdminAuthenticate(Login login, out string databaseMessage);

    }

    public class LoginDatabase : ILoginDatabase
    {
        //declaration des variables
        private MySqlConnection connection;
        private string connectionString;
        User user;
        Admin admin;

        public LoginDatabase()
        {
            //connectionString = "server=localhost;user=root;password=I@mgoingincounterstrike1.6fin@L;database=rdv";
            connectionString = "server=localhost;user=root;password=;database=rdv";
            connection = new MySqlConnection(connectionString);
        }

        public User LoginUser(Login login, out string databaseMessage)
        {
            string command = "SELECT * FROM `utilisateur` WHERE email = '"+login.email+"' AND mot_de_passe = '"+login.password+"';";
            return GetUserMethod(command, out databaseMessage);
        }

        public Admin LoginAdmin(Login login, out string databaseMessage)
        {
            string command = "SELECT * FROM `Administrateur` WHERE email = '" + login.email + "' AND mot_de_passe = '" + login.password + "';";
            return GetAdminMethod(command, out databaseMessage);
        }

        private User GetUserMethod(string commandd, out string databaseMessage)
        {
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    user = new User();
                    user.id = Convert.ToInt32(reader.GetValue(0));
                    user.lastName = reader.GetValue(1).ToString();
                    user.firstName = reader.GetValue(2).ToString();
                    user.birthDate = reader.GetValue(3).ToString();
                    user.numberPhone = reader.GetValue(4).ToString();
                    user.email = reader.GetValue(5).ToString();
                    user.role = reader.GetValue(7).ToString();
                    user.numberReservation = Convert.ToByte(reader.GetValue(8));
                    user.numberMissedAppointment = Convert.ToByte(reader.GetValue(9));
                    user.userState = reader.GetValue(10).ToString();
                    user.stateDescription = reader.GetValue(11).ToString();
                }
                if (admin == null)
                    databaseMessage = "Email Or Password Is Incorrect.";
                else
                    databaseMessage = "Admin Login Done With Success.";
            }
            catch (Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return user;
        }

        private Admin GetAdminMethod(string commandd, out string databaseMessage)
        {

            using var command = new MySqlCommand(commandd, connection);
            try
            {
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    admin = new Admin();
                    admin.id = Convert.ToInt32(reader.GetValue(0));
                    admin.lastName = reader.GetValue(1).ToString();
                    admin.firstName = reader.GetValue(2).ToString();
                    admin.birthDate = reader.GetValue(3).ToString();
                    admin.numberPhone = reader.GetValue(4).ToString();
                    admin.email = reader.GetValue(5).ToString();
                    admin.role = reader.GetValue(7).ToString();
                }
                if(admin == null)
                    databaseMessage = "Email Or Password Is Incorrect.";
                else
                    databaseMessage = "Admin Login Done With Success.";
            }
            catch (Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return admin;
        }

        public User UserAuthenticate(Login login, out string databaseMessage)
        {
            user = LoginUser(login, out databaseMessage);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("Adil Arhrimaz And Oumaima Sabi's Project");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.id.ToString()),
                    new Claim(ClaimTypes.Role, user.role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.token = tokenHandler.WriteToken(token);

            return user;
        }

        public Admin AdminAuthenticate(Login login, out string databaseMessage)
        {
            admin = LoginAdmin(login, out databaseMessage);

            // return null if user not found
            if (admin == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("Adil Arhrimaz And Oumaima Sabi's Project");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, admin.id.ToString()),
                    new Claim(ClaimTypes.Role, admin.role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            admin.token = tokenHandler.WriteToken(token);

            return admin;
        }
        //ouverture de la connexion
        public string OpenConnection()
        {
            try
            {
                connection.Open();
                return "Successful Connection.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //fermeture de la connexion
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}

