﻿using MySql.Data.MySqlClient;
using RDV.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Database
{
    public class SoltDatabase
    {
        // declaration des variables
        private MySqlConnection connection;
        private string connectionString;
        List<Solt> solts;
        Solt solt;

        public SoltDatabase()
        {
            connectionString = "server=localhost;user=root;password=I@mgoingincounterstrike1.6fin@L;database=rdv";
            //connectionString = "server=localhost;user=root;password=;database=rdv";
            connection = new MySqlConnection(connectionString);
        }

        public List<Solt> GetActiveSolt(string date, bool soltState, out string databaseMessage)
        {
            solts = new List<Solt>();
            string command = "SELECT * FROM creneau WHERE etat_creneau = "+Convert.ToInt32(soltState)+";";
            solts = GetMethod(command, date, out databaseMessage);
            foreach (var solt in solts)
            {
                solt.percentage = GetSoltPercentage(solt.idSolt, date);
            }
            return solts;

        }

        public List<Solt> GetSoltByState(bool soltState, out string databaseMessage)
        {
            string command = "SELECT * FROM creneau WHERE etat_creneau = " + Convert.ToInt32(soltState) + ";";
            return GetMethod2(command, out databaseMessage);
        }

        public List<Solt> GetSolt(out string databaseMessage)
        {
            string command = "SELECT * FROM creneau;";
            return GetMethod2(command, out databaseMessage);
        }


        public List<Solt> GetUsefulSolt(out string databaseMessage)
        {
            solts = new List<Solt>();
            var numbers = new List<int>();
            var i = 0;
            var ids = GetIdSolt();
            try
            {
                foreach (var id in ids)
                {
                    using var command = new MySqlCommand("SELECT COUNT(*) FROM rdv WHERE id_creneau = " + id + ";", connection);
                    using var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        numbers.Add(Convert.ToInt32(reader.GetValue(0)));
                    }

                }
                foreach (var id in ids)
                {
                    using var command = new MySqlCommand("SELECT * FROM creneau WHERE id_creneau = " + id + ";", connection);
                    using var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        solt = new Solt();
                        solt.idSolt = Convert.ToInt32(reader.GetValue(0));
                        solt.duration = Convert.ToInt32(reader.GetValue(1));
                        solt.durationStart = reader.GetValue(2).ToString();
                        solt.percentage = Convert.ToByte(numbers[i]);
                        solt.soltState = Convert.ToBoolean(reader.GetValue(3));
                        solts.Add(solt);
                        i++;
                    }


                }
                databaseMessage = "Solt Process done with success.";
            }
            catch(Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return solts;
        }

        public string EditSoltState(int id, bool soltState)
        {
            string command = "UPDATE `creneau` SET `etat_creneau`="+Convert.ToInt32(soltState)+" WHERE ";
            return SetMethod(command);
        }

        public string SetSolt(Solt solt)
        {
            string command = "INSERT INTO `creneau`(`duree`, `debut_duree`) VALUES("+solt.duration+", '"+solt.durationStart+"');";
            return SetMethod(command);
        }

        private List<int> GetIdSolt()
        {
            var ids = new List<int>();
            string commandd = "SELECT id_creneau FROM creneau WHERE etat_creneau = 1;";
            using var command = new MySqlCommand(commandd, connection);
            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                ids.Add(Convert.ToInt32(reader.GetValue(0)));
            }
            return ids;
        }

        private List<Solt> GetMethod(string commandd, string date, out string databaseMessage)
        {

            solts = new List<Solt>();


            using var command = new MySqlCommand(commandd, connection);
            try
            {
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    solt = new Solt();
                    solt.idSolt = Convert.ToInt32(reader.GetValue(0));
                    solt.duration = Convert.ToInt32(reader.GetValue(1));
                    solt.durationStart = reader.GetValue(2).ToString();
                    //solt.percentage = GetSoltPercentage(solt.idSolt, date);
                    solt.soltState = Convert.ToBoolean(reader.GetValue(3));
                    solts.Add(solt);
                }
                foreach(var solt in solts)
                {
                    solt.percentage = GetSoltPercentage(solt.idSolt, date);
                }
                databaseMessage = "Solt Process done with success.";
            }
            catch(Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return solts;
        }

        private List<Solt> GetMethod2(string commandd, out string databaseMessage)
        {

            solts = new List<Solt>();


            using var command = new MySqlCommand(commandd, connection);
            try
            {
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    solt = new Solt();
                    solt.idSolt = Convert.ToInt32(reader.GetValue(0));
                    solt.duration = Convert.ToInt32(reader.GetValue(1));
                    solt.durationStart = reader.GetValue(2).ToString();
                    solt.soltState = Convert.ToBoolean(reader.GetValue(3));
                    solts.Add(solt);
                }
                databaseMessage = "Solt Process done with success.";
            }
            catch (Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return solts;
        }


        private byte GetSoltPercentage(int id, string date)
        {
            byte percentage = 0 ;
            using var command = new MySqlCommand("SELECT COUNT(*) FROM rdv WHERE etat_rdv = 'on hold' AND date_rdv = '" + date + "' AND id_creneau = " + id + ";", connection);
            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                    percentage = Convert.ToByte(((1.00f / (Convert.ToInt32(reader.GetValue(0)) + 1)) * 100));
                      
            }
            return percentage;
        }

        public void SetSoltHandler(List<Setting> settings)
        {
            var times = new List<DateTime>();
            var duration = 0;
            var isDuration = false;
            var isTime = false;
            var commandd = "";
            OpenConnection();
            foreach(var setting in settings)
            {
                if (setting.id == 3)
                {
                    duration = Convert.ToInt32(setting.values[0]);
                    isDuration = true;
                }
                else if (setting.id == 4)
                {
                    foreach(var time in setting.values)
                    {
                        times.Add(DateTime.ParseExact(time, "HH:mm:ss", CultureInfo.InvariantCulture));
                    }
                    isTime = true;
                }
            }
            if(isDuration == true && isTime == false)
            {
                commandd = "SELECT valeur FROM valeur WHERE id_parametre = 4 AND etat_parametre = 1;";
                using var command = new MySqlCommand(commandd, connection);
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    times.Add(DateTime.ParseExact(reader.GetValue(0).ToString(), "HH:mm:ss", CultureInfo.InvariantCulture));
                }
                SetSolt(times, duration);
            }
            else if (isTime == true && isDuration == false)
            {
                commandd = "SELECT valeur FROM valeur WHERE id_parametre = 3 AND etat_parametre = 1;";
                using var command = new MySqlCommand(commandd, connection);
                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    duration = Convert.ToInt32(reader.GetValue(0));
                }
                SetSolt(times, duration);
            }
            else if(isTime == true && isDuration == true)
            {
                SetSolt(times, duration);
            }
            CloseConnection();
            
        }

        private void SetSolt(List<DateTime> times, int duration)
        {
            
            var command = "UPDATE creneau SET etat_creneau = 0 WHERE etat_creneau = 1;";
            string error;
            error = SetMethod(command);
            var time = times[0];
            while ( time < times[1])
            {
                command = "INSERT INTO creneau(`duree`, `debut_duree`, `etat_creneau`) VALUES("+duration+",'"+time.ToString("HH:mm:ss") + "',1);";
                error = SetMethod(command);
                time = time.AddMinutes(duration);
            }
            time = times[2];
            while (time < times[3])
            {
                command = "INSERT INTO creneau(`duree`, `debut_duree`, `etat_creneau`) VALUES(" + duration + ",'" + time.ToString("HH:mm:ss") + "',1);";
                SetMethod(command);
                time = time.AddMinutes(duration);
            }
            
        }

        private string SetMethod(string commandd)
        {
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                command.ExecuteNonQuery();
                return "Solt Added / Edited / Deleted Successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //ouverture de la connexion 
        public string OpenConnection()
        {
            try
            {
                connection.Open();
                return "Successful Connection.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //fermeture de la connexion
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
