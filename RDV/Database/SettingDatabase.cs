﻿using MySql.Data.MySqlClient;
using RDV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDV.Database
{
    public class SettingDatabase
    {
        private MySqlConnection connection;
        private string connectionString;
        List<Setting> settings;
        Setting setting;
        SoltDatabase solt;
        public SettingDatabase()
        {
            connectionString = "server=localhost;user=root;password=I@mgoingincounterstrike1.6fin@L;database=rdv";
            //connectionString = "server=localhost;user=root;password=;database=rdv";
            connection = new MySqlConnection(connectionString);
        }



        public string SetSetting(List<Setting> settings)
        {
            solt = new SoltDatabase();
            var error = "";
            try
            {
                foreach (var setting in settings)
                {
                    string command = "UPDATE `valeur` SET `etat_parametre` = 0 WHERE id_parametre = " + setting.id + " AND etat_parametre = 1;";
                    SetMethod(command);
                    foreach (var value in setting.values)
                    {
                        command = "INSERT INTO `valeur`(`id_parametre`, `valeur`, `date_ajout`, `etat_parametre`) VALUES (" + setting.id + ",'" + value + "','" + setting.addedDate + "',1);";
                        error = SetMethod(command);
                    }
                    
                }
                solt.SetSoltHandler(settings);
            }
            catch(Exception ex)
            {
                error = ex.Message;
            }
            return error;
        }

      

        public List<Setting> GetActiveSetting(out string databaseMessage)
        {

            settings = new List<Setting>();
            try
            {
                for (int i = 1; i <= 8; i++)
                {
                    using var command = new MySqlCommand("SELECT parametre.id_parametre, parametre.nom_parametre, valeur.valeur, valeur.date_ajout FROM parametre, valeur WHERE parametre.id_parametre = valeur.id_parametre AND valeur.etat_parametre = 1 AND parametre.id_parametre = " + i + ";", connection);
                    using var reader = command.ExecuteReader();
                    var j = 0;
                    var settingValues = new List<string>();
                    setting = new Setting();
                    while (reader.Read())
                    {
                        if (j == 0)
                        {
                            setting.id = Convert.ToInt32(reader.GetValue(0));
                            setting.name = reader.GetValue(1).ToString();
                            settingValues.Add(reader.GetValue(2).ToString());
                            setting.addedDate = reader.GetValue(3).ToString();
                            j++;
                        }
                        else
                        {
                            settingValues.Add(reader.GetValue(2).ToString());
                        }
                    }
                    setting.values = settingValues;
                    settings.Add(setting);
                }
                databaseMessage = "Setting Process done with success.";
            }
            catch(Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return settings;
        }

        public List<Setting> GetInitialSetting(out string databaseMessage)
        {

            settings = new List<Setting>();

            try
            {

                for (int i = 1; i <= 8; i++)
                {
                    using var command = new MySqlCommand("SELECT parametre.id_parametre, parametre.nom_parametre, valeur.valeur, valeur.date_ajout FROM parametre, valeur WHERE parametre.id_parametre = valeur.id_parametre AND valeur.date_ajout ='1000-01-01 00:00:00:' AND parametre.id_parametre = " + i + ";", connection);
                    using var reader = command.ExecuteReader();
                    var j = 0;
                    var settingValues = new List<string>();
                    setting = new Setting();
                    while (reader.Read())
                    {
                        if (j == 0)
                        {
                            setting.id = Convert.ToInt32(reader.GetValue(0));
                            setting.name = reader.GetValue(1).ToString();
                            settingValues.Add(reader.GetValue(2).ToString());
                            setting.addedDate = reader.GetValue(3).ToString();
                            j++;
                        }
                        else
                        {
                            settingValues.Add(reader.GetValue(2).ToString());
                        }
                    }
                    setting.values = settingValues;
                    settings.Add(setting);
                }
                databaseMessage = "Setting Process done with success.";
            }
            catch(Exception ex)
            {
                databaseMessage = ex.Message;
            }
            return settings;
        }

       

        public List<Setting> GetSettingByModificationDate(string date, out string databaseMessage)
        {
            var commands = new List<string>();
            settings = new List<Setting>();
            commands.Add("SELECT DISTINCT parametre.id_parametre, parametre.nom_parametre, valeur.valeur, valeur.date_ajout FROM parametre, valeur,modification WHERE parametre.id_parametre = valeur.id_parametre AND parametre.id_parametre = modification.id_parametre AND valeur.date_ajout = '" + date + "' ;");
            commands.Add("SELECT DISTINCT parametre.id_parametre, parametre.nom_parametre, valeur.valeur, valeur.date_ajout FROM parametre, valeur , modification WHERE parametre.id_parametre = valeur.id_parametre AND modification.id_parametre = valeur.id_parametre AND parametre.id_parametre IN (SELECT id_parametre FROM modification WHERE date_modification = '" + date + "') AND valeur.date_ajout = (SELECT MAX(date_ajout) from valeur where id_parametre IN (SELECT id_parametre FROM modification WHERE date_modification = '" + date + "') and date_ajout < '" + date + "')");
            try
            {
                foreach (var comm in commands)
                {
                    using var command = new MySqlCommand(comm, connection);
                    using var reader = command.ExecuteReader();
                    bool check = false;
                    int id = 0;
                    var settingValues = new List<string>();

                    while (reader.Read())
                    {
                        if (id != Convert.ToInt32(reader.GetValue(0)))
                        {
                            if (check)
                            {
                                setting.values = settingValues;
                                settings.Add(setting);
                            }
                            setting = new Setting();
                            settingValues = new List<string>();
                            setting.id = Convert.ToInt32(reader.GetValue(0));
                            setting.name = reader.GetValue(1).ToString();
                            settingValues.Add(reader.GetValue(2).ToString());
                            setting.addedDate = reader.GetValue(3).ToString();
                            id = Convert.ToInt32(reader.GetValue(0));
                            check = true;
                        }
                        else
                        {
                            settingValues.Add(reader.GetValue(2).ToString());
                        }
                    }
                    setting.values = settingValues;
                    settings.Add(setting);
                }
                databaseMessage = "Setting Process done with success.";
            }
            catch(Exception ex)
            {
                databaseMessage = ex.Message;
            }
            
            return settings;
        }

        private string SetMethod(string commandd)
        {
            
            using var command = new MySqlCommand(commandd, connection);
            try
            {
                command.ExecuteNonQuery();
                return "Setting Added / Edited / Deleted Successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public string OpenConnection()
        {
            try
            {
                connection.Open();
                return "Successful Connection.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public void CloseConnection()
        {
            connection.Close();
        }
    }
}
