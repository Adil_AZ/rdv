using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace RDV
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
           Host.CreateDefaultBuilder(args)
               .ConfigureWebHostDefaults(webBuilder =>
               {
                   //var cert = new X509Certificate2(Path.Combine("./fulldraws.com.pfx"), "I@mgoingincounterstrike1.6fin@L");
                   webBuilder.UseStartup<Startup>();
                   /*.ConfigureKestrel(options =>
                   {
                       options.Limits.MinRequestBodyDataRate = null;
                       options.ConfigureHttpsDefaults(o =>
                       {
                           o.ServerCertificate = cert;
                           //o.ClientCertificateMode = ClientCertificateMode.RequireCertificate;
                       });
                       options.Listen(IPAddress.Parse("185.181.10.230"), 5000, listenOptions =>
                       {
                           listenOptions.UseHttps(cert);
                           listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
                       });

                   });*/

               });
        


    }
}
